(function(){

var filas,
	cols,
	bolas,
	$turno = $('.turno'),
	$tablero = $('.tablero'),
	$numFilas = $('#numFilas'),
	$numCols = $('#numCols'),
	$numBolas = $('#numBolas');

function calculaMaxBolas(){
	var numCols = $(this).children(':selected').val(),
		i = 3,
		maxBolas = 	parseInt(numCols/2,10);

	$numBolas.children(':first').siblings().remove();
	for(; i<=maxBolas; i++){
		$numBolas
			.append($('<option />',{
				text:i,
				value:i
			}));
	}
}

function creaTablero(){
	var i = 1,
		j,
		huecos,
		columnaAleatoria,
		bolasR,
		bolasA;
	//Limpiamos el tablero anterior
	$tablero.children().remove();

	for(; i <= filas; i++){
		$tablero
			.append($('<div />',{
				'class':'fila'
				//id:'fila'+i
			}));
		var $fila = $('.fila').eq(i-1);//$('#fila'+i);
		for(j=1;j<=cols;j++){
			$fila
				.append($('<div />',{
					'class':'bloque',
					id:'col'+i+j
				}));
		}
		if(i < 3){
			if(bolas % 2 === 0){
				bolasR = bolasA = bolas / 2;
			}
			else{
				bolasA = i;
				bolasR = 3 - bolasA;
			}

			while(bolasR > 0){
				columnaAleatoria = Math.floor(Math.random()*cols);
				if($fila.children(':eq('+columnaAleatoria+')').hasClass('bloque')){
					//$('#col'+i+columnaAleatoria)
					$fila.children(':eq('+columnaAleatoria+')')
						.removeClass('bloque')
						.addClass('bolaRoja');
					bolasR--;
				}
			}
			while(bolasA > 0){
				columnaAleatoria = Math.floor(Math.random()*cols);
				if($fila.children(':eq('+columnaAleatoria+')').hasClass('bloque')){
					$fila.children(':eq('+columnaAleatoria+')')
						.removeClass('bloque')
						.addClass('bolaAmarilla');
					bolasA--;
				}
			}
		}
		else if(i === 3){
			huecos = bolas;
			while(huecos > 0){
				columnaAleatoria = Math.floor(Math.random()*cols);
				if($fila.children(':eq('+columnaAleatoria+')').hasClass('bloque') && !($fila.prev().children(':eq('+columnaAleatoria+')').is('.bolaRoja, .bolaAmarilla'))){
					$fila.children(':eq('+columnaAleatoria+')')
						.removeClass('bloque')
						.addClass('hueco');
					huecos--;
				}
			}
		}
		else{
			huecos = bolas;
			while(huecos > 0){
				columnaAleatoria = Math.floor(Math.random()*cols);
				if($fila.children(':eq('+columnaAleatoria+')').hasClass('bloque')){
					$fila.children(':eq('+columnaAleatoria+')')
						.removeClass('bloque')
						.addClass('hueco');
					huecos--;
				}
			}
		}
	}
}

function muestraFlechas(jugadorTurno){
	var bolasJugador = (jugadorTurno === 1)?'.bolaRoja':'.bolaAmarilla',
		i = 1;

	for(; i<filas; i++){
		var $fila = $('.fila').eq(i-1); //$('#fila' + i);
		if($fila.children(bolasJugador).length > 0){
			if($fila.children('.flechaDer').length === 0){
				$fila
					.prepend($('<div />',{'class':'flechaDer'}).data({fila: i, sentido: 'der'}))
					.append($('<div />',{'class':'flechaIz'}).data({fila: i, sentido: 'iz'}));
			}
		}
		else if($fila.children('.flechaDer').length > 0){
			$fila.children('.flechaDer, .flechaIz').remove();
		}
	}
}

function rotaFila(fila,sentido){
	var $fila = $('.fila').eq(fila-1), //$('#fila' + fila),
		claseGuardada,
		i;
	if(sentido === 'der'){
		claseGuardada = $fila.children(':nth-child('+(cols+1)+')').attr('class');
		for(i=cols+1; i>2; i--){
			$fila.children(':nth-child('+ i +')')
				.removeClass()
				.addClass($fila.children(':nth-child('+(i-1)+')').attr('class'));
		}
		$fila.children(':nth-child(2)')
			.removeClass()
			.addClass(claseGuardada);
	}
	else{
		claseGuardada = $fila.children(':nth-child(2)').attr('class');
		for(i=2; i<=cols; i++){
			$fila.children(':nth-child('+ i +')')
				.removeClass()
				.addClass($fila.children(':nth-child('+(i+1)+')').attr('class'));
		}
		$fila.children(':nth-child('+(cols+1)+')')
			.removeClass()
			.addClass(claseGuardada);
	}	
}

function gravedad(){
	var i = 1,
		j,
		k,
		$casilla,
		$fila;

	for(;i<=cols;i++){
		for(j = filas; j>1; j--){
			$fila = $('.fila').eq(j-1);
			$casilla = $('#col' + j + i);
			if($casilla.hasClass('hueco')){
				//Ultima fila
				k = j-1;
				if(j === filas){
					while(k >= 1 && !$('#col' + k + i).hasClass('bloque')){
						$('#col' + k + i)
							.removeClass()
							.addClass('hueco');
						k--;
					}
				}
				else{
					while(k >= 1 && !$('#col' + k + i).hasClass('bloque')){
						if($('#col' + k + i).is('.bolaRoja, .bolaAmarilla')){
							$casilla
								.removeClass()
								.addClass($('#col' + k + i).attr('class'));
							$('#col' + k + i)
								.removeClass()
								.addClass('hueco');
							break;
						}
						k--;			
					}
				}
			}
		}
	}
}

function ganador(){
	var ganador = 0,
		numBolasR = $('.bolaRoja').length,
		numBolasA = $('.bolaAmarilla').length;

	if(numBolasA === 0){
		ganador = 2;
	}
	else if(numBolasR === 0){
		ganador = 1;
	}

	return ganador;
}

function finPartida(jugadorGanador){
	$('.flechaDer, .flechaIz').remove();
	$turno
		.removeClass('jugador1 jugador2')
		.addClass('jugador' + jugadorGanador)
		.children('p')
		.text('Winner Jugador ' + jugadorGanador + '!!!!');
	//alert('El jugador ' + jugadorGanador + ' es el ganador.');
	$("<p>El jugador '" + jugadorGanador + "' es el ganador.</p>").dialog({
		modal: true,
		title: 'Ganador',
		width: 340,
		dialogClass: "alert"
	});
}

//Rellena las opciones de los select
for(var i = 5; i<=8; i++){
	$numFilas.append($('<option />',{
		value:i,
		text:i
	}));
	$numCols.append($('<option />',{
		value:i,
		text:i
	}));
}
$numBolas.append($('<option />',{
		value:2,
		text:2
	}));

//Calcula el numero de bolas posibles en función de las columnas
$('#numCols').on('change', calculaMaxBolas);
//Crea el tablero
$('#nuevoJuego').on('click',function(){
	filas = parseInt($numFilas.children(':selected').val());
	cols = parseInt($numCols.children(':selected').val());
	bolas = parseInt($('#numBolas').children(':selected').val());
	creaTablero();
	muestraFlechas(1);
	$turno
		.removeClass('jugador2')
		.addClass('jugador1')
		.show()
		.children('p')
		.text('Jugador 1');
});
//Manejador click en flechas
$tablero.on('click','.flechaDer, .flechaIz',function(event){
	var fila = $(event.target).data('fila'),
		proxJugador,
		jugadorGanador,
		sentido = $(event.target).data('sentido');
	
	rotaFila(fila, sentido);
	gravedad();
	jugadorGanador = ganador();
	if(jugadorGanador > 0){
		finPartida(jugadorGanador);
	}
	else{
		if($turno.children('p').text() === 'Jugador 1'){
			proxJugador = 2;
		}
		else{
			proxJugador = 1;
		}
		muestraFlechas(proxJugador);
		$turno
			.toggleClass('jugador1 jugador2')
			.children('p')
			.text('Jugador ' + proxJugador);
	}
});

})();


